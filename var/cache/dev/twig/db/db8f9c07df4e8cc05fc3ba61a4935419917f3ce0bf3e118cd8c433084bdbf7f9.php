<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_19458b94aa6e345460f14e5094c6e876ffdb2812a6da05d7a78ad8172bf61cfc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_32391d6d6dd0e8513b207b028ab84957934bceac3c84ca35303f13f9f00d2f23 = $this->env->getExtension("native_profiler");
        $__internal_32391d6d6dd0e8513b207b028ab84957934bceac3c84ca35303f13f9f00d2f23->enter($__internal_32391d6d6dd0e8513b207b028ab84957934bceac3c84ca35303f13f9f00d2f23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_32391d6d6dd0e8513b207b028ab84957934bceac3c84ca35303f13f9f00d2f23->leave($__internal_32391d6d6dd0e8513b207b028ab84957934bceac3c84ca35303f13f9f00d2f23_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_b04baf3d63ed6ded0ad4b823afd160a316c4796c911dd3d5054a4d2b1b0939ca = $this->env->getExtension("native_profiler");
        $__internal_b04baf3d63ed6ded0ad4b823afd160a316c4796c911dd3d5054a4d2b1b0939ca->enter($__internal_b04baf3d63ed6ded0ad4b823afd160a316c4796c911dd3d5054a4d2b1b0939ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_b04baf3d63ed6ded0ad4b823afd160a316c4796c911dd3d5054a4d2b1b0939ca->leave($__internal_b04baf3d63ed6ded0ad4b823afd160a316c4796c911dd3d5054a4d2b1b0939ca_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_9b1b84e91dfc90f1fb8f787926a5eddeb2e111d1c95d2f3652d2a31e1145d895 = $this->env->getExtension("native_profiler");
        $__internal_9b1b84e91dfc90f1fb8f787926a5eddeb2e111d1c95d2f3652d2a31e1145d895->enter($__internal_9b1b84e91dfc90f1fb8f787926a5eddeb2e111d1c95d2f3652d2a31e1145d895_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_9b1b84e91dfc90f1fb8f787926a5eddeb2e111d1c95d2f3652d2a31e1145d895->leave($__internal_9b1b84e91dfc90f1fb8f787926a5eddeb2e111d1c95d2f3652d2a31e1145d895_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_3d654dc2a071e8c0634ceb6283e0dd9336c0a5522dca0cd2e74b701e94a77354 = $this->env->getExtension("native_profiler");
        $__internal_3d654dc2a071e8c0634ceb6283e0dd9336c0a5522dca0cd2e74b701e94a77354->enter($__internal_3d654dc2a071e8c0634ceb6283e0dd9336c0a5522dca0cd2e74b701e94a77354_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_3d654dc2a071e8c0634ceb6283e0dd9336c0a5522dca0cd2e74b701e94a77354->leave($__internal_3d654dc2a071e8c0634ceb6283e0dd9336c0a5522dca0cd2e74b701e94a77354_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */

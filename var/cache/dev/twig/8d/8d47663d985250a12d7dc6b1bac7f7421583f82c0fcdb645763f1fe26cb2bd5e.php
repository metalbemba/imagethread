<?php

/* base.html.twig */
class __TwigTemplate_9000aeb43442cf8c76bd438b7db21edc02dac7664ba013059d8e4c47fd41784d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f6e2adf2b8710b981ce2851faa75f367decd52aabc284472543f356f99b9275c = $this->env->getExtension("native_profiler");
        $__internal_f6e2adf2b8710b981ce2851faa75f367decd52aabc284472543f356f99b9275c->enter($__internal_f6e2adf2b8710b981ce2851faa75f367decd52aabc284472543f356f99b9275c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
    ";
        // line 12
        $this->displayBlock('body', $context, $blocks);
        // line 13
        $this->displayBlock('javascripts', $context, $blocks);
        // line 14
        echo "</body>
</html>
";
        
        $__internal_f6e2adf2b8710b981ce2851faa75f367decd52aabc284472543f356f99b9275c->leave($__internal_f6e2adf2b8710b981ce2851faa75f367decd52aabc284472543f356f99b9275c_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_74c4d1f0277435198f848788a0398a81e6a1da844d56d1a759433600d31ca7ad = $this->env->getExtension("native_profiler");
        $__internal_74c4d1f0277435198f848788a0398a81e6a1da844d56d1a759433600d31ca7ad->enter($__internal_74c4d1f0277435198f848788a0398a81e6a1da844d56d1a759433600d31ca7ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "ImageThread";
        
        $__internal_74c4d1f0277435198f848788a0398a81e6a1da844d56d1a759433600d31ca7ad->leave($__internal_74c4d1f0277435198f848788a0398a81e6a1da844d56d1a759433600d31ca7ad_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_4411b5407e7ec92217691971dcd0856f68931aeb4402cef4d2081b8f38a79000 = $this->env->getExtension("native_profiler");
        $__internal_4411b5407e7ec92217691971dcd0856f68931aeb4402cef4d2081b8f38a79000->enter($__internal_4411b5407e7ec92217691971dcd0856f68931aeb4402cef4d2081b8f38a79000_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        echo "            
            <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/imagethread.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" />
        ";
        
        $__internal_4411b5407e7ec92217691971dcd0856f68931aeb4402cef4d2081b8f38a79000->leave($__internal_4411b5407e7ec92217691971dcd0856f68931aeb4402cef4d2081b8f38a79000_prof);

    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        $__internal_ca9ab1d2e85310d9117bd2cf6d5708baa4796c11461a43253bd5983a22d5e0bc = $this->env->getExtension("native_profiler");
        $__internal_ca9ab1d2e85310d9117bd2cf6d5708baa4796c11461a43253bd5983a22d5e0bc->enter($__internal_ca9ab1d2e85310d9117bd2cf6d5708baa4796c11461a43253bd5983a22d5e0bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_ca9ab1d2e85310d9117bd2cf6d5708baa4796c11461a43253bd5983a22d5e0bc->leave($__internal_ca9ab1d2e85310d9117bd2cf6d5708baa4796c11461a43253bd5983a22d5e0bc_prof);

    }

    // line 13
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_c47ccff03f52981902300d25b441f38ffc4b7b0d5fe6a0aaba9b3cdecaa2b0c1 = $this->env->getExtension("native_profiler");
        $__internal_c47ccff03f52981902300d25b441f38ffc4b7b0d5fe6a0aaba9b3cdecaa2b0c1->enter($__internal_c47ccff03f52981902300d25b441f38ffc4b7b0d5fe6a0aaba9b3cdecaa2b0c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_c47ccff03f52981902300d25b441f38ffc4b7b0d5fe6a0aaba9b3cdecaa2b0c1->leave($__internal_c47ccff03f52981902300d25b441f38ffc4b7b0d5fe6a0aaba9b3cdecaa2b0c1_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 13,  87 => 12,  78 => 7,  70 => 6,  58 => 5,  49 => 14,  47 => 13,  45 => 12,  38 => 9,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}ImageThread{% endblock %}</title>*/
/*         {% block stylesheets %}            */
/*             <link href="{{ asset('css/imagethread.css') }}" rel="stylesheet" type="text/css" />*/
/*         {% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*     {% block body %}{% endblock %}*/
/* {% block javascripts %}{% endblock %}*/
/* </body>*/
/* </html>*/
/* */

<?php

/* default/index.html.twig */
class __TwigTemplate_d1cf40b5a8527281128c27ac8b0eddfd6ea15b57320d366373c88b70cdb5d5aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_64da158258d96f1637a3745fb6c005e39403cf599ebced306e5b2bd83e53e8c7 = $this->env->getExtension("native_profiler");
        $__internal_64da158258d96f1637a3745fb6c005e39403cf599ebced306e5b2bd83e53e8c7->enter($__internal_64da158258d96f1637a3745fb6c005e39403cf599ebced306e5b2bd83e53e8c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_64da158258d96f1637a3745fb6c005e39403cf599ebced306e5b2bd83e53e8c7->leave($__internal_64da158258d96f1637a3745fb6c005e39403cf599ebced306e5b2bd83e53e8c7_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_34bd613fb5731bbf746862c83885e16b7aabd277d04e8b5db8d51132a8095778 = $this->env->getExtension("native_profiler");
        $__internal_34bd613fb5731bbf746862c83885e16b7aabd277d04e8b5db8d51132a8095778->enter($__internal_34bd613fb5731bbf746862c83885e16b7aabd277d04e8b5db8d51132a8095778_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div id=\"wrapper\">
        <div id=\"container\">
            <div id=\"top\">
                <div id=\"posts\">Posts: ";
        // line 7
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["entries"]) ? $context["entries"] : $this->getContext($context, "entries"))), "html", null, true);
        echo "<span class=\"quantity\"></span></div>
                <div id=\"export\">
                    ";
        // line 9
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["formexport"]) ? $context["formexport"] : $this->getContext($context, "formexport")), 'form_start', array("attr" => array("id" => "form_export", "target" => "_blank")));
        echo "
                    ";
        // line 10
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["formexport"]) ? $context["formexport"] : $this->getContext($context, "formexport")), 'widget');
        echo "
                    ";
        // line 11
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["formexport"]) ? $context["formexport"] : $this->getContext($context, "formexport")), 'form_end');
        echo "
                </div>
                <div id=\"views\">Views: ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["views"]) ? $context["views"] : $this->getContext($context, "views")), "views", array()), "html", null, true);
        echo "<span class=\"quantity\"></span></div>
            </div>
            <div id=\"replybox\">
                ";
        // line 16
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("id" => "form_entry")));
        echo "
                ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
                ";
        // line 18
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
            </div>
            <div id=\"entries\">
                ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entries"]) ? $context["entries"] : $this->getContext($context, "entries")));
        foreach ($context['_seq'] as $context["_key"] => $context["entry"]) {
            // line 22
            echo "                    <div class=\"entry\">
                        <div class=\"title\">
                            ";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "title", array()), "html", null, true);
            echo "
                        </div>
                        <div class=\"image\">
                            <img src=\"uploaded/";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "image", array()), "html", null, true);
            echo "\" />
                        </div>    
                        <div class=\"time\">
                            ";
            // line 30
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "date", array()), "Y-m-d H:i:s"), "html", null, true);
            echo "
                        </div>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "            </div>
        </div>
    </div>
";
        
        $__internal_34bd613fb5731bbf746862c83885e16b7aabd277d04e8b5db8d51132a8095778->leave($__internal_34bd613fb5731bbf746862c83885e16b7aabd277d04e8b5db8d51132a8095778_prof);

    }

    // line 39
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_531b621c9eec3acc30753d849e1c4488f335e0166fcaca61d062835bbbdfe104 = $this->env->getExtension("native_profiler");
        $__internal_531b621c9eec3acc30753d849e1c4488f335e0166fcaca61d062835bbbdfe104->enter($__internal_531b621c9eec3acc30753d849e1c4488f335e0166fcaca61d062835bbbdfe104_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 40
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/jquery-1.12.3.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/imagethread.js"), "html", null, true);
        echo "\"></script>    
";
        
        $__internal_531b621c9eec3acc30753d849e1c4488f335e0166fcaca61d062835bbbdfe104->leave($__internal_531b621c9eec3acc30753d849e1c4488f335e0166fcaca61d062835bbbdfe104_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 41,  130 => 40,  124 => 39,  114 => 34,  104 => 30,  98 => 27,  92 => 24,  88 => 22,  84 => 21,  78 => 18,  74 => 17,  70 => 16,  64 => 13,  59 => 11,  55 => 10,  51 => 9,  46 => 7,  41 => 4,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <div id="wrapper">*/
/*         <div id="container">*/
/*             <div id="top">*/
/*                 <div id="posts">Posts: {{ entries|length }}<span class="quantity"></span></div>*/
/*                 <div id="export">*/
/*                     {{ form_start(formexport, {'attr': {'id': 'form_export','target':'_blank'}}) }}*/
/*                     {{ form_widget(formexport) }}*/
/*                     {{ form_end(formexport) }}*/
/*                 </div>*/
/*                 <div id="views">Views: {{ views.views }}<span class="quantity"></span></div>*/
/*             </div>*/
/*             <div id="replybox">*/
/*                 {{ form_start(form, {'attr': {'id': 'form_entry'}}) }}*/
/*                 {{ form_widget(form) }}*/
/*                 {{ form_end(form) }}*/
/*             </div>*/
/*             <div id="entries">*/
/*                 {%for entry in entries %}*/
/*                     <div class="entry">*/
/*                         <div class="title">*/
/*                             {{ entry.title }}*/
/*                         </div>*/
/*                         <div class="image">*/
/*                             <img src="uploaded/{{ entry.image }}" />*/
/*                         </div>    */
/*                         <div class="time">*/
/*                             {{ entry.date|date('Y-m-d H:i:s') }}*/
/*                         </div>*/
/*                     </div>*/
/*                 {% endfor %}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block javascripts %}*/
/*     <script src="{{ asset('js/jquery-1.12.3.min.js') }}"></script>*/
/*     <script src="{{ asset('js/imagethread.js') }}"></script>    */
/* {% endblock %}*/
/* */
/* */

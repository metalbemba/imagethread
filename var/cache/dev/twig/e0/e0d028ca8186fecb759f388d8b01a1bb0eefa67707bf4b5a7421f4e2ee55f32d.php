<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_32d545af70d38575ac9ae0de41c0b11dc5af6a4f04454d216c094f0f4c77d5ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c10b20e02cc6a2295175e140b48b170ae2b118c41ea37bf23e3005788a4cc61e = $this->env->getExtension("native_profiler");
        $__internal_c10b20e02cc6a2295175e140b48b170ae2b118c41ea37bf23e3005788a4cc61e->enter($__internal_c10b20e02cc6a2295175e140b48b170ae2b118c41ea37bf23e3005788a4cc61e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c10b20e02cc6a2295175e140b48b170ae2b118c41ea37bf23e3005788a4cc61e->leave($__internal_c10b20e02cc6a2295175e140b48b170ae2b118c41ea37bf23e3005788a4cc61e_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_d24bcebef5665e49bf20c3b8dee591a27ae97eee5799e6c30856ff59263ea1cc = $this->env->getExtension("native_profiler");
        $__internal_d24bcebef5665e49bf20c3b8dee591a27ae97eee5799e6c30856ff59263ea1cc->enter($__internal_d24bcebef5665e49bf20c3b8dee591a27ae97eee5799e6c30856ff59263ea1cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_d24bcebef5665e49bf20c3b8dee591a27ae97eee5799e6c30856ff59263ea1cc->leave($__internal_d24bcebef5665e49bf20c3b8dee591a27ae97eee5799e6c30856ff59263ea1cc_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_bb1bba7e3122796429b1a435c3990ac4078ad94870c887d16af5b59537ae9cff = $this->env->getExtension("native_profiler");
        $__internal_bb1bba7e3122796429b1a435c3990ac4078ad94870c887d16af5b59537ae9cff->enter($__internal_bb1bba7e3122796429b1a435c3990ac4078ad94870c887d16af5b59537ae9cff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_bb1bba7e3122796429b1a435c3990ac4078ad94870c887d16af5b59537ae9cff->leave($__internal_bb1bba7e3122796429b1a435c3990ac4078ad94870c887d16af5b59537ae9cff_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_3aeeaf833bee76e8be3762ace8d67cc267ba4b1bbe664a2bccaf7c1091f5eadb = $this->env->getExtension("native_profiler");
        $__internal_3aeeaf833bee76e8be3762ace8d67cc267ba4b1bbe664a2bccaf7c1091f5eadb->enter($__internal_3aeeaf833bee76e8be3762ace8d67cc267ba4b1bbe664a2bccaf7c1091f5eadb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_3aeeaf833bee76e8be3762ace8d67cc267ba4b1bbe664a2bccaf7c1091f5eadb->leave($__internal_3aeeaf833bee76e8be3762ace8d67cc267ba4b1bbe664a2bccaf7c1091f5eadb_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */

<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Entry;
use AppBundle\Entity\Views;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        $entry = new Entry();

        $em = $this->getDoctrine()->getManager();

        // We obtain the views to show them
        $views = $this->getDoctrine()
                ->getRepository('AppBundle:Views')
                ->find(1);

        // We're going to increase the views while displaying our page with the get method
        if ($request->isMethod('GET')) {
            if (is_null($views)) {
                // First entry
                $views = new Views();
                $views->setViews(1);
                $em->persist($views);
                $em->flush();
            } else {
                // We increase the number of views
                $views->setViews($views->getViews() + 1);
                $em->persist($views);
                $em->flush();
            }
        }

        //We define the form for the export button
        $formexport = $this->createFormBuilder()
                ->setAction($this->generateUrl('export'))
                ->add('export', SubmitType::class, array('label' => 'Export'))
                ->getForm();

        //We define the form for the reply box
        $form = $this->createFormBuilder($entry)
                ->add('title', TextType::class)
                ->add('image', FileType::class)
                ->getForm();


        $form->handleRequest($request);

        try {
            if ($form->isSubmitted() && $form->isValid()) {

                $file = $form['image']->getData();

                //We verfiy the mime type
                if (!empty($file)) {
                    $ext = $file->guessExtension();
                    if (!in_array(strtolower($ext), array("jpg", "jpeg", "png", "gif"))) {
                        $form->addError(new FormError('Only jpg or png images allowed.'));
                    }
                    // We verify the size
                    if ($file->getClientSize() > 20971520) {
                        $form->addError(new FormError('Max file size (20Mb) exceeded.'));
                    }
                }

                $path = uniqid() . "." . $file->guessExtension();
                $file->move($this->container->getParameter('kernel.root_dir') . '/../web/uploaded/', $path);

                $properties = getimagesize($this->container->getParameter('kernel.root_dir') . '/../web/uploaded/' . $path);

                // We verify the dimensions
                if ($properties[0] > 1920 || $properties[1] > 1080) {
                    throw new \Exception("Max image dimensions should not exceed 1920x1080 px.", 200);
                }

                $entry->setImage($path);

                if ($entry->getTitle() === NULL) {
                    $entry->setTitle("");
                }
                $time = new \Datetime("now");
                $entry->setDate($time);

                //We save the post
                $em->persist($entry);
                $em->flush();
            }
        } catch (\Exception $ex) {
            $form->addError(new FormError($ex->getMessage()));
        }

        //We get all the entries to show them
        $entries = $this->getDoctrine()
                ->getRepository('AppBundle:Entry')
                ->findBy(array(), array('date' => 'DESC'));

        return $this->render('default/index.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),
                    'form' => $form->createView(),
                    'formexport' => $formexport->createView(),
                    'entries' => $entries,
                    'views' => $views,
        ]);
    }

    /**
     * @Route("/export", name="export")
     */
    public function exportAction(Request $request) {
        $entry = new Entry();
        
        //We get all the entries to obtain the posts
        $entries = $this->getDoctrine()
                ->getRepository('AppBundle:Entry')
                ->findBy(array(), array('date' => 'DESC'));

        //route to the csv files
        $path = $this->getParameter('kernel.root_dir') . '/../src/AppBundle/Resources/tmp/export.csv';

        file_put_contents($path, "Title;Filename\n");

        //We write the csv file
        foreach ($entries as $entry) {
            file_put_contents($path, $entry->getTitle() . ";" . $entry->getImage() . "\n", FILE_APPEND);
        }

        $zip = new \ZipArchive();
        $filename = $this->getParameter('kernel.root_dir') . '/../src/AppBundle/Resources/tmp/export.zip';

        //If the zip file exists we delete it
        if (file_exists($filename)) {
            unlink($filename);
        }

        try {
            //We create the zip file
            if ($zip->open($filename, \ZipArchive::CREATE) !== TRUE) {
                throw new Exception("cannot open <$filename>\n");
            }

            //We read the images directory
            if ($dir = opendir($this->container->getParameter('kernel.root_dir') . '/../web/uploaded/')) {
                while (($file = readdir($dir)) != FALSE) {
                    if ($file != "." && $file != "..") {
                        $zip->addFile($this->container->getParameter('kernel.root_dir') . '/../web/uploaded/' . $file, $file);
                    }
                }

                closedir($dir);
            }
            
            //We add the csv to the zip
            $zip->addFile($this->getParameter('kernel.root_dir') . '/../src/AppBundle/Resources/tmp/export.csv', "export.csv");
            $zip->close();

            $response = new BinaryFileResponse($filename);
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

            return $response;
        } catch (\Exception $ex) {
            $response = new Response($ex->getMessage());
            return $response;
        }
    }

}
